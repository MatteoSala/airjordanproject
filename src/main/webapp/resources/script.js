function registerFieldsValidator(event) {
		event.preventDefault();
		var form = document.registrati;
		var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
		if(form.nome.value.trim()==""){
			alert("Il nome non puo' essere lasciato vuoto");
	    	form.nome.focus();
	    	return false;
	    }else if(form.cognome.value.trim()==""){
	    	alert("Il cognome non puo' essere lasciato vuoto");
	    	form.cognome.focus();
	    	return false;
	    }else if(form.email.value.trim()==""){
	    	alert("L'email non puo' essere lasciata vuota");
	    	form.email.focus();
	    	return false;
	    }else if (!regex.test(form.email.value)){
	    	alert("L'email non e' valida");
	    	form.email.focus();
	    	return false;
	    }else if(form.indirizzo.value.trim()==""){
	    	alert("L'indirizzo non puo' essere lasciato vuoto");
	    	form.indirizzo.focus();
	    	return false;
	    }else if(form.citta.value.trim()==""){
	    	alert("La citta' non puo' essere lasciata vuota");
	    	form.citta.focus();
	    	return false;
	    }else if(form.telefono.value.trim()==""){
	    	alert("Il telefono non puo' essere lasciato vuoto");
	    	form.telefono.focus();
	    	return false;
	    }else if(form.password.value.trim()==""){
	    	alert("La password non puo' essere lasciata vuota");
	    	form.password.focus();
	    	return false;
	    }else if(form.cPassword.value.trim()==""){
	    	alert("La password di conferma non puo' essere lasciata vuota");
	    	form.cPassword.focus();
	    	return false;
	    }else if(form.cPassword.value!=form.password.value){
	    	alert("Le due password non coincidono");
	    	form.cPassword.focus();
	    	return false;
	    }else
	    	form.submit();
}
function loginFieldsValidator(event){
	event.preventDefault();
	var form = document.entra;
	
	if(form.username.value.trim()==""){
		alert("L'username non puo' essere lasciato vuoto");
    	form.username.focus();
    	return false;
    }else if(form.password.value.trim()==""){
    	alert("La password non puo' essere lasciata vuota");
    	form.password.focus();
    	return false;
    }else
    	form.submit();
}