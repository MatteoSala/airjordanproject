<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/script.js" />"></script>
<title>AIR JORDAN | LOGIN</title>
</head>
<body>
	<div style="height:95%;">
		<h1 class="one">
			<span>
				<a href="<%=request.getContextPath()%>/">
					<img src="../images/logo.jpg" class="images">
				</a>
			</span>
		</h1>
		<div class="clear"></div>
		<div style="width:100%;" class="inputFields">
			<!-- LOGIN -->
			<div class="login">
				<form name="entra" action="<%=request.getContextPath()%>/loginController/loginValidator" onSubmit="return loginFieldsValidator(event);" method="POST">
					<input type="text" name="username" placeholder="E-mail" required/><br>
					<input type="password" name="password" placeholder="Password" required/><br><br>
					<input type="submit" id="loginBtn" value="login" class="submitFlat" style="margin-top: 3.1em;"/>
				</form>
			</div>
			<!-- REGISTRATI -->
			<div class="registrati">
				<form style="border-left:2px solid #ff0000;" name="registrati" action="<%=request.getContextPath()%>/loginController/registration" onSubmit="return registerFieldsValidator(event);" method="POST">
					<div class="scrollingForm">
						<input type="text" name="nome" placeholder="Nome" required/><br>
						<input type="text" name="cognome" placeholder="Cognome" required/><br>
						<input type="text" name="email" placeholder="E-mail" required/><br>
						<input type="text" name="indirizzo" placeholder="Indirizzo" required/><br>
						<input type="text" name="citta" placeholder="Citta'" required/><br>
						<input type="text" name="telefono" placeholder="Telefono" required/><br>
						<input type="password" name="password" placeholder="Password" required/><br>
						<input type="password" name="cPassword" placeholder="Conferma Password" required/><br><br>
					</div>
						<input type="hidden" name="form" value="register">
						<input type="submit" id="registratiBtn" value="registrati" class="submitFlat"/>
				</form>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="footer">
		<p class="paragrafo">Privacy Policy :: Contact &copy; 2017 Air Jordan</p>
	</div>
</body>
</html>