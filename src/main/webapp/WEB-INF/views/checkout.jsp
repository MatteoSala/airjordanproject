<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<title>AIR JORDAN | CHECKOUT</title>
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script>
$(document).ready(function(){
	var spese = 10;
	var totaleP = parseFloat($("#totaleP").text()); 
	$("#spese").html("<b>Spese di spedizione:</b> "+spese+" &euro;");
	$("#totale").html("<b>TOTALE:</b> "+(totaleP+spese)+" &euro;");
});
</script>
</head>
<body>
	<div style="height:95%;">
		<h1 class="one">
			<a href="<%=request.getContextPath()%>/loginController/logout" class="logout"><img src="../images/logout.png"><br>LOGOUT</a>
			<span>
				<a href="<%=request.getContextPath()%>/">
					<img src="../images/logo.jpg" class="images">
				</a>
			</span>
		</h1>
		
		<div class="clear"></div>
		
		<div class="summaryDiv color1">
			Dati Utente:<br><br><br>
			<div class="summaryDiv2">
		        <span><b>Nome:</b> ${customerData.nome}</span><br><br>
				<span><b>Cognome:</b> ${customerData.cognome}</span><br><br>
				<span><b>Indirizzo di spedizione:</b> ${customerData.indirizzo}</span><br><br>
				<span><b>Citta':</b> ${customerData.citta}</span><br><br>
				<span><b>Telefono:</b> ${customerData.telefono}</span>	
			</div>
		</div>
		
		<div class="summaryDiv color2">
			Riepilogo Ordine:<br><br><br>
			<div class="summaryDiv2">
				<span><b>Numero articoli:</b> ${nItems}</span><br><br>
				<span><b>Totale parziale:</b> <span id="totaleP">${totaleParziale}</span> &euro;</span><br><br>
				<span id="spese"></span><br><br>
				<div style="width:91%; border-top:2px solid black;"></div><br>
				<span id="totale"></span>
			</div><br><br>
			<input type="button" value="Torna al Carrello" class="button lightblue" style="margin-right:3em;" onclick="window.location= '<%=request.getContextPath()%>/cartController/cartSummary'";/>
			<input type="button" value="Conferma" class="button green" onclick="window.location= '<%=request.getContextPath()%>/checkoutController/checkoutConfirm'"/>
		</div>

		<div class="clear"></div>
	</div>
	<div class="footer">
		<p class="paragrafo">Privacy Policy :: Contact &copy; 2017 Air Jordan</p>
	</div>
</body>
</html>