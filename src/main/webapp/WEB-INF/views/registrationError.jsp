<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<title>AIR JORDAN | ERROR</title>
</head>
<body>
	<div style="height:95%;">
		<h1 class="one">
			<span>
				<a href="<%=request.getContextPath()%>/">
					<img src="../images/logo.jpg" class="images">
				</a>
			</span>
		</h1>
		<div class="clear"></div>
		
		<div class="welcome">Oops! Si e' verificato un errore!<br>
			 Questo errore puo' essere causato dall'inserimento di una mail gia' utilizzata<br>
			 oppure non si riescono a inserire i dati in fase di registrazione.<br>
			 Si prega di riprovare<br><br>
			<a href="<%=request.getContextPath()%>/loginController/login">
				<input type="button" id="loginBtn" value="vai alla login" class="submitFlat"/>
			</a>
		</div>
		
		<div class="clear"></div>
	</div>
	<div class="footer">
		<p class="paragrafo">Privacy Policy :: Contact &copy; 2017 Air Jordan</p>
	</div>
</body>
</html>