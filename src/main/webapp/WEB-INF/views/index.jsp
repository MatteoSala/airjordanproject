<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<title>AIR JORDAN | HOME</title>
<script>
function checkSession(category){
	<% if(session.getAttribute("username")==null) { %>
		window.location= "<%=request.getContextPath()%>/loginController/login";
	<% } else { %>
		window.location= "<%=request.getContextPath()%>/catalogPage?category="+category;
	<% } %>
	
}
</script>

</head>
<body>
	<div style="height:95%;">
		<h1 class="one">
			<span>
				<img src="images/logo.jpg" class="images">
			</span>
		</h1>
		<div class="clear"></div>
		<div class="descrizione"> 
			<p class="intro"><i>Benvenuti nel sito ufficiale AIR JORDAN!</i></p>
			<p class="testo">La Air Jordan � un brand lanciato da Nike nel 1985, 
			ispirata al campione di pallacanestro NBA <b><i>Michael "Air" Jordan</i></b>, 
			chiamato cos� per le sue eccezionali doti atletiche ed il gioco aereo spettacolare.<br><br>
			Qui potrai trovare tutti i capi di abbigliamento Jordan: T-Shirt, Cappelli, Scarpe e Pantaloni.
			</p>
		</div>
		<div class="category">
			<a href="javascript:void(0);" onclick="checkSession('cappelli')">
				<div class="square">
					<div class="hiddenDiv"><br>Cappelli</div>
					<img src="images/cap.png" class="centerImg" style="padding-top: 10px;">
				</div>
			</a>
			<a href="javascript:void(0);" onclick="checkSession('tshirts')">
				<div class="square">
					<div class="hiddenDiv"><br>T-Shirts</div>
					<img src="images/tshirt.png" class="centerImg" style="max-height: 85%; padding-top: 5px;">
				</div>
			</a>
			<div class="clear"></div>
			<a href="javascript:void(0);" onclick="checkSession('pantaloni')">
				<div class="square">
					<div class="hiddenDiv"><br>Pantaloni</div>
					<img src="images/shorts.png" class="centerImg" style="height: 7em; width: 8em; padding-top: 5px;">
				</div>
			</a>
			<a href="javascript:void(0);" onclick="checkSession('scarpe')">
				<div class="square">
					<div class="hiddenDiv"><br>Scarpe</div>
					<img src="images/shoes.png" class="centerImg" style="max-height: 75%; padding-top: 20px;">
				</div>
			</a>
		</div>
		<div class="clear"></div>
	</div>
	<div class="footer">
		<p class="paragrafo">Privacy Policy :: Contact &copy; 2017 Air Jordan</p>
	</div>
</body>
</html>