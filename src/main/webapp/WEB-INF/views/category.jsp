<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>AIR JORDAN | <%=request.getParameter("category").toUpperCase() %></title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script>
$(document).ready(function(){
	radiobtn = document.getElementById('${category}');
	radiobtn.checked = true;
	$('input[type=radio][name=category]').change(function() {
		var category = document.querySelector('input[name = "category"]:checked').value;
		window.location= "<%=request.getContextPath()%>/catalogPage?category="+category;
    });
});

function addToCart(idProdotto){
	 $.ajax({
		  url: "<%=request.getContextPath()%>/itemsController",
		  type: "POST",
		  data: {productID: idProdotto},
		  success: function(responseText){
			  $('.cartBadge').text(responseText);
		  },
		  error:function(){
		      alert("Errore nell'aggiunta al carrello!");
		  }
		});
};

</script>
</head>
<body>
	<div style="height:95%;">
		<h1 class="one">
			<a href="<c:url value="/loginController/logout" />" class="logout"><img src="images/logout.png"><br>LOGOUT</a>
			<span>
				<a href="<%=request.getContextPath()%>/">
					<img src="images/logo.jpg" class="images">
				</a>
			</span>
			<a href="<%=request.getContextPath()%>/cartController/cartSummary" class="cartLink">		
				<span aria-hidden="true" class="cartBadge"><%= request.getAttribute("nItems") %></span>
				<span aria-hidden="true" style="padding: 0;">
					<img src="images/cart.png" style="width:1.2em;" />
					<label>Carrello</label>
				</span>
			</a>
		</h1>
		<div class="clear"></div>
		
		<div class="categoryContent">
			<ul class="categoryType">
				<li>
					<input type="radio" id="tshirts" name="category" value="tshirts" checked="checked"/>
					<label for="tshirts">T-Shirts</label>
				</li>
				<li>
					<input type="radio" id="cappelli" name="category" value="cappelli" />
					<label for="cappelli">Cappelli</label>
				</li>
				<li>
					<input type="radio" id="scarpe" name="category" value="scarpe" />
					<label for="scarpe">Scarpe</label>
				</li>
				<li>
					<input type="radio" id="pantaloni" name="category" value="pantaloni" />
					<label for="pantaloni">Pantaloni</label>
				</li>
			</ul>
		</div>
		
		<div class="tableDiv">
			<table>
				<tr>
					<th></th>
					<th>Descrizione</th>
					<th>Prezzo</th>
					<th style="border-right:0;"></th>
					</tr>
					<c:forEach items="${productsList}" var="product" varStatus="status">
				        <tr>
				            <td><img src="DBImages/${product.immagine}" style="width: 65px;"></td>
				            <td>${product.descrizione}</td>
				            <td>${product.prezzo}&euro;</td>
				            <td><button id="loginBtn" class="submitFlat addToCart" value="${product.id}" onclick="addToCart(this.value);">AGGIUNGI AL CARRELLO</button></td>
				        </tr>
				    </c:forEach>
			</table>
		</div>
		
		<div class="clear"></div>
	</div>
	<div class="footer">
		<p class="paragrafo">Privacy Policy :: Contact &copy; 2017 Air Jordan</p>
	</div>
</body>
</html>