package com.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;


@Component
public class CartItemDAOImpl implements CartItemDAO{
	
	@Resource(name="dataSource")
	private javax.sql.DataSource myDB;
	
	@Override
	public int insertProductToCart(int cartID, int productID) throws SQLException {
		// TODO Auto-generated method stub
		String query = null;
		Connection c = myDB.getConnection();
		PreparedStatement ps = null;
		if(checkExistingProduct(productID,cartID)){
			query = "UPDATE cart_item SET quantity = quantity + 1 WHERE id_product = ? AND id_cart = ?";
			ps = c.prepareStatement(query);
	    	ps.setInt(1, productID);
	    	ps.setInt(2, cartID);
		}else{
			query = "INSERT INTO cart_item (id_product, id_cart, quantity) VALUES(?,?,1)";
			ps = c.prepareStatement(query);
	    	ps.setInt(1, productID);
	    	ps.setInt(2, cartID);
		}
    	int rs = ps.executeUpdate();
        if(rs!=0){
        	c.close();
        	return getAllProductsQuantity(cartID);
        }else{
        	c.close();
        	return -1;
        }
	}
	
	@Override
	public boolean checkExistingProduct(int productID,int cartID) throws SQLException{
		String query = "SELECT * FROM cart_item WHERE id_product = ? AND id_cart = ?";
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query);
    	ps.setInt(1, productID);
    	ps.setInt(2, cartID);
    	ResultSet rs = ps.executeQuery();
    	if(!rs.next()){
    		c.close();
			return false; //NON HA TROVATO NIENTE
    	}else{
    		c.close();
    		return true;
    	}
	}

	@Override
	public int getAllProductsQuantity(int cartID) throws SQLException{
		String query = "SELECT SUM(quantity) FROM cart_item WHERE id_cart = ?";
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query);
    	ps.setInt(1, cartID);
    	ResultSet rs = ps.executeQuery();
    	int numberRow = 0;
    	while(rs.next()){
            numberRow = rs.getInt(1);
        }
    	c.close();
    	return numberRow;
	}
	
	@Override
	public int selectQuantityByIdCartItem(int id_cartItem, String action) throws SQLException {
		if(updateQuantityByIdCartItem(id_cartItem, action)){
			String query = "SELECT quantity FROM cart_item WHERE idcart_item = ?";
			Connection c = myDB.getConnection();
	    	PreparedStatement ps = c.prepareStatement(query);
	    	ps.setInt(1, id_cartItem);
	    	ResultSet rs = ps.executeQuery();
	    	int quantity = 0;
	    	while(rs.next()){
	            quantity = rs.getInt(1);
	            if(quantity==0)
	            	deleteCartItem(id_cartItem);
	        }
	    	c.close();
	    	return quantity;
		}
		return -1;
	}
	
	@Override
	public boolean updateQuantityByIdCartItem(int id_cartItem, String action) throws SQLException {
		String operation = null;
		if(action.equals("plus"))
			operation = "+ 1";
		else
			operation = "- 1";
		
		StringBuilder query = new StringBuilder();
		query.append("UPDATE cart_item SET quantity = quantity ");
		query.append(operation);
		query.append(" WHERE idcart_item = ?");
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query.toString());
    	ps.setInt(1, id_cartItem);
    	int rs = ps.executeUpdate();
        if(rs!=0){
        	c.close();
        	return true;
        }else{
        	c.close();
        	return false;
        }
	}
	
	@Override
	public boolean deleteCartItem(int id_cartItem) throws SQLException {
		String query = "DELETE FROM cart_item WHERE idcart_item = ?";
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query);
    	ps.setInt(1, id_cartItem);
    	int rs = ps.executeUpdate();
        if(rs!=0){
        	c.close();
        	return true;
        }else{
        	c.close();
        	return false;
        }
	}
}
