package com.core.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import com.core.model.CustomerModel;

public interface CustomerDAO {
	boolean getCustomer(String email, String password, HttpSession session) throws SQLException;
	boolean insertCustomer(CustomerModel cm) throws SQLException;
	CustomerModel getCustomer(int customerID) throws SQLException;
}
