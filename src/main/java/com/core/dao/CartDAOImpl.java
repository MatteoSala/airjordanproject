package com.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

@Component
public class CartDAOImpl implements CartDAO{
	
	@Resource(name="dataSource")
	private javax.sql.DataSource myDB;
	
	@Override
	public int selectExistCart(int customerID) throws SQLException {
		// TODO Auto-generated method stub
		String query = "SELECT cartID FROM cart WHERE customerID = ? AND closed = 'false'";
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query);
    	ps.setInt(1, customerID);
    	ResultSet rs = ps.executeQuery();
    	if(!rs.next()){
    		c.close();
			return 0; //NON HA TROVATO NIENTE
    	}else{
			int cartID = rs.getInt(1);
			c.close();
			return cartID;
		}
	}

	@Override
	public boolean createCartForCustomer(int customerID) throws SQLException {
		// TODO Auto-generated method stub
		String query = "INSERT INTO cart (customerID,closed) VALUES(?,'false')";
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query);
    	ps.setInt(1, customerID);
    	int rs = ps.executeUpdate();
        if(rs!=0){
        	c.close();
        	return true;
        }else{
        	c.close();
        	return false;
        }
	}

	@Override
	public boolean deleteCartByCustomer(int customerID) throws SQLException {
		String query = "DELETE FROM cart WHERE customerID = ? AND closed = 'false'";
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query);
    	ps.setInt(1, customerID);
    	int rs = ps.executeUpdate();
        if(rs!=0){
        	c.close();
        	return true;
        }else{
        	c.close();
        	return false;
        }
	}

	@Override
	public boolean setClosedCart(int customerID) throws SQLException {
		String query = "UPDATE cart SET closed = 'true' WHERE customerID = ? AND closed = 'false'";
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query);
    	ps.setInt(1, customerID);
    	int rs = ps.executeUpdate();
        if(rs!=0){
        	c.close();
        	return true;
        }else{
        	c.close();
        	return false;
        }
	}

}
