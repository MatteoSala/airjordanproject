package com.core.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.annotation.Resource;
import org.hibernate.*;
import org.springframework.stereotype.Component;
import com.core.model.CartModel;
import com.core.model.ProductModel;
import com.core.service.HibernateUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Component
public class ProductDAOImpl implements ProductDAO{
	
	@Resource(name="dataSource")
	private javax.sql.DataSource myDB;
	
	@Override
	public ArrayList<ProductModel> getProductListByCategory(String category) throws SQLException {
		final Session session = HibernateUtil.getSessionFactory().openSession();
		
		try{
			Query query = session.createQuery("FROM ProductModel WHERE categoria = '"+category+"'");
			@SuppressWarnings("unchecked")
			ArrayList<ProductModel> list = (ArrayList<ProductModel>) query.list();
			return list;
		  }catch (Exception ex) {
		    // Log the exception here
			  ex.printStackTrace();
		  }finally {
			  session.close();
			  //HibernateUtil.shutdown();
		}
		return null;
	}

	@Override
	public ArrayList<CartModel> getSummaryProductCartList(int customerID) throws SQLException {
		CartModel cartModel = null;
		ArrayList<CartModel> list = new ArrayList<CartModel>();
		String query = "SELECT CI.idcart_item,P.foto,P.descrizione,P.prezzo,CI.quantity FROM ((products P INNER JOIN cart_item CI ON P.idproduct = CI.id_product)INNER JOIN cart C ON CI.id_cart = C.cartID) WHERE C.customerID = ? AND C.closed = 'false'";
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query);
    	ps.setInt(1, customerID);
    	ResultSet rs = ps.executeQuery();
    	while(rs.next()){
    		cartModel = new CartModel();
    		cartModel.setId(rs.getInt("idcart_item"));
    		cartModel.setFoto(rs.getString("foto"));
    		cartModel.setDescrizione(rs.getString("descrizione"));
    		cartModel.setPrezzo(rs.getFloat("prezzo"));
    		cartModel.setQuantita(rs.getInt("quantity"));
    		list.add(cartModel);
    	}
    	c.close();
    	return list;
	}

}
