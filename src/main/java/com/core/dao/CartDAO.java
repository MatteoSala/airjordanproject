package com.core.dao;

import java.sql.SQLException;

public interface CartDAO {
	int selectExistCart(int customerID) throws SQLException;
	boolean createCartForCustomer(int customerID) throws SQLException;
	boolean deleteCartByCustomer(int customerID) throws SQLException;
	boolean setClosedCart(int customerID) throws SQLException;
}
