package com.core.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import com.core.model.CartModel;
import com.core.model.ProductModel;

public interface ProductDAO {
	ArrayList<ProductModel> getProductListByCategory(String category) throws SQLException;
	ArrayList<CartModel> getSummaryProductCartList(int customerID) throws SQLException;
}
