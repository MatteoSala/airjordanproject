package com.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Component;
import com.core.model.CustomerModel;

@Component
public class CustomerDAOImpl implements CustomerDAO{

	@Resource(name="dataSource")
	private javax.sql.DataSource myDB;
	
	@Resource(name="customerModel")
	private CustomerModel cm;
	
	@Override
	public boolean getCustomer(String email, String password, HttpSession session) throws SQLException {
		String query = "SELECT * FROM customers WHERE email = ? AND password = ?";
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query);
    	ps.setString(1, email);
    	ps.setString(2, password);
    	ResultSet rs = ps.executeQuery();
    	if(!rs.next()){
    		c.close();
			return false; //NON HA TROVATO NIENTE
    	}else{
			int idCustomer = rs.getInt(1); 
			session.setAttribute("idCustomer", idCustomer);
			c.close();
			return true;
		}
	}

	@Override
	public boolean insertCustomer(CustomerModel model) throws SQLException {
		if(!checkExistingEmail(model.getEmail())){
			String query = "INSERT INTO customers(nome,cognome,email,indirizzo,citta,telefono,password) VALUES (?,?,?,?,?,?,?)";
			Connection c = myDB.getConnection();
	    	PreparedStatement ps = c.prepareStatement(query);
	    	ps.setString(1, model.getNome());
	    	ps.setString(2, model.getCognome());
	    	ps.setString(3, model.getEmail());
	    	ps.setString(4, model.getIndirizzo());
	    	ps.setString(5, model.getCitta());
	    	ps.setString(6, model.getTelefono());
	    	ps.setString(7, model.getPassword());
	    	int rs = ps.executeUpdate();
	        if(rs!=0){
	        	c.close();
	        	return true;
	        }else{
	        	c.close();
	        	return false;
	        }
		}
		else{
			return false;
		}
	}
	
	public boolean checkExistingEmail(String email) throws SQLException{
		String query = "SELECT * FROM customers WHERE email='"+email+"'";
		Connection c = myDB.getConnection();
    	Statement statement = c.createStatement();
    	ResultSet rs = statement.executeQuery(query);
    	if(!rs.next()){
    		c.close();
			return false; //NON HA TROVATO NIENTE
    	}else{
    		c.close();
			return true;
    	}
	}

	public CustomerModel getCustomer(int customerID) throws SQLException {
		String query = "SELECT * FROM customers WHERE idcustomers = ?";
		Connection c = myDB.getConnection();
    	PreparedStatement ps = c.prepareStatement(query);
    	ps.setInt(1, customerID);
    	ResultSet rs = ps.executeQuery();
    	if(rs.next()){
    		cm.setId(rs.getInt("idcustomers"));
    		cm.setNome(rs.getString("nome"));
    		cm.setCognome(rs.getString("cognome"));
    		cm.setEmail(rs.getString("email"));
    		cm.setTelefono(rs.getString("telefono"));
    		cm.setCitta(rs.getString("citta"));
    		cm.setIndirizzo(rs.getString("indirizzo"));
    		cm.setPassword(rs.getString("password"));
    		c.close();
    		return cm;
    	}else{
			return null;
		}
	}
	

}
