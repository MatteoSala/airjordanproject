package com.core.dao;

import java.sql.SQLException;

public interface CartItemDAO {
	int insertProductToCart(int cartID, int productID) throws SQLException;
	int getAllProductsQuantity(int cartID) throws SQLException;
	boolean checkExistingProduct(int productID, int cartID) throws SQLException;
	int selectQuantityByIdCartItem(int id_cartItem, String action) throws SQLException;
	boolean updateQuantityByIdCartItem(int id_cartItem, String action) throws SQLException;
	boolean deleteCartItem(int id_cartItem) throws SQLException;
}
