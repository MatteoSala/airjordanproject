package com.core.model;

public class CustomerModel {
	private int id;
	private String nome;
	private String cognome;
	private String email;
	private String telefono;
	private String indirizzo;
	private String citta;
	private String password;
	
	public CustomerModel(){}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "CustomerModel [nome=" + nome + ", cognome=" + cognome + ", email=" + email + ", telefono=" + telefono
				+ ", indirizzo=" + indirizzo + ", citta=" + citta + ", password=" + password + "]";
	}
	
	
	
}
