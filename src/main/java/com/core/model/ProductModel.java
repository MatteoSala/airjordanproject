package com.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity()
@Table(name="products")
public class ProductModel {

	@Id@GeneratedValue
	@Column(name = "idproduct")
	private int id;
	
	@Column(name = "foto")
	private String immagine;
	
	@Column(name = "descrizione")
	private String descrizione;
	
	@Column(name = "categoria")
	private String categoria;
	
	@Column(name = "prezzo")
	private double prezzo;
	
	public ProductModel(){}
	
	public ProductModel(int id, String immagine, String descrizione, double prezzo){
		this.id = id;
		this.immagine = immagine;
		this.descrizione = descrizione;
		this.prezzo = prezzo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	@Override
	public String toString() {
		return "Product [id=" + id +" immagine=" + immagine + ", descrizione=" + descrizione + ", prezzo=" + prezzo + "]";
	}
}
