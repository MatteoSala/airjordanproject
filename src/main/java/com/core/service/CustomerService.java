package com.core.service;

import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.core.dao.CustomerDAOImpl;
import com.core.model.CustomerModel;

public class CustomerService {
	@Resource(name="customerDAOImpl")
	private CustomerDAOImpl cdi;
	
	@Resource(name="hashingMD5")
	private HashingPasswordMD5 encrypt;
	
	public boolean getCustomerForLogin(String email, String password, HttpSession session) throws SQLException{
		String hashedPsw = encrypt.hashingMD5(password);
		return cdi.getCustomer(email, hashedPsw, session);
	}

	public boolean addCustomer(CustomerModel cm) throws SQLException {
		return cdi.insertCustomer(cm);
	}

	public CustomerModel getCustomerForCheckout(int customerID) throws SQLException {
		return cdi.getCustomer(customerID);
	}
}
