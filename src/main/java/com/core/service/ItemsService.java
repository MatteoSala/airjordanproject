package com.core.service;

import java.sql.SQLException;
import javax.annotation.Resource;
import com.core.dao.CartDAOImpl;
import com.core.dao.CartItemDAOImpl;

public class ItemsService {

	@Resource(name = "cartItemDAOImpl")
	private CartItemDAOImpl cartItemDAOImpl;
	
	@Resource(name = "cartDAOImpl")
	private CartDAOImpl cartDAOImpl;
	
	public int addToCart(int productID, int customerID) throws SQLException{
		int cartID = cartDAOImpl.selectExistCart(customerID);
		if(cartID==0){
			if(cartDAOImpl.createCartForCustomer(customerID)){
				cartID = cartDAOImpl.selectExistCart(customerID);
				return cartItemDAOImpl.insertProductToCart(cartID,productID);
			}
		}else{
			return cartItemDAOImpl.insertProductToCart(cartID,productID);
		}
		return -1;
	}

	public int getNCartItems(int customerID) throws SQLException {
		int cartID = cartDAOImpl.selectExistCart(customerID);
		return cartItemDAOImpl.getAllProductsQuantity(cartID);
	}

	public int getQuantityByIdCartItem(int id_cartItem, String action) throws SQLException {
		return cartItemDAOImpl.selectQuantityByIdCartItem(id_cartItem, action);
	}
}
