package com.core.service;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.annotation.Resource;
import com.core.dao.CartDAOImpl;
import com.core.dao.ProductDAOImpl;
import com.core.model.CartModel;
import com.core.model.ProductModel;
import com.facade.data.CartData;

public class ProductService {

	@Resource(name="productDAOImpl")
	private ProductDAOImpl pdi;
	@Resource(name="cartDAOImpl")
	private CartDAOImpl cdi;

	public ArrayList<ProductModel> getCatalogsByCategory(String category) throws SQLException{
		return pdi.getProductListByCategory(category);
		
	}
	
	public ArrayList<CartModel> getSummaryCart(int customerID) throws SQLException {
		return pdi.getSummaryProductCartList(customerID);
	}

	public boolean removeCartByCustomer(int customerID) throws SQLException {
		return cdi.deleteCartByCustomer(customerID);
	}
	
	public boolean closeCartByCustomer(int customerID) throws SQLException {
	return cdi.setClosedCart(customerID);
}
	
//	public ArrayList<ProductModel> getCatalogsByCustomer(int customerID) throws SQLException {
//		return pdi.getProductListByCustomer(customerID);
//	}

}
