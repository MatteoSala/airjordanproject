package com.web.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.core.model.ProductModel;
import com.facade.facades.ItemsFacade;
import com.facade.facades.ProductFacade;

@Controller
public class CatalogController {
	
	@Resource(name="productFacade")
	private ProductFacade pf;
	
	@Resource(name="itemsFacade")
	private ItemsFacade itemFacade;
	
	@RequestMapping(value = "/catalogPage", method = RequestMethod.GET)
	protected ModelAndView getCatalogByCategory(
			@RequestParam("category") String category,
			HttpServletRequest request, 
			HttpServletResponse response){
		ModelAndView mv = null;
		HttpSession session = request.getSession();
		if(session.getAttribute("idCustomer")!=null){
			int customerID = (int) session.getAttribute("idCustomer");
			int nItemsOfCart = 0;
			
			ArrayList<ProductModel> productsList = null;
			try {
				productsList = pf.getProductServiceByCategory(category);
				nItemsOfCart = itemFacade.getCartService(customerID);
				
				mv = new ModelAndView("category");
				mv.addObject("category", category);
				mv.addObject("productsList", productsList);
				mv.addObject("nItems", nItemsOfCart);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mv = new ModelAndView("redirect:/");
			}
			//request.setAttribute("nItems", nItemsOfCart);
		}else{
			mv = new ModelAndView("redirect:/loginController/login");
		}
		return mv;
	}

}
