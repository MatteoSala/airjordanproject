package com.web.controller;

import java.sql.SQLException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.facade.data.CustomerData;
import com.facade.facades.CustomerFacade;

@Controller
@RequestMapping("/loginController")
public class LoginController{
	
	@Resource(name="customerFacade")
	private CustomerFacade cmFacade;
	
	@Resource(name="customerData")
	private CustomerData data;
	
	@RequestMapping("/login")
	protected String getLoginView(){
		// TODO Auto-generated method stub
		return "login";
	}
	
	@RequestMapping("/logout")
	protected String getLogout(HttpServletRequest request){
		HttpSession session = request.getSession();
		session.invalidate();
		return "redirect:/";
	}
	
	@RequestMapping("/loginValidator")
	protected String checkLoginValidation(
			@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "password", required = false) String password,
			HttpServletRequest request){
		
		String view = null;
		try {
				HttpSession session = request.getSession();
				if(cmFacade.getCustomerServiceForLogin(username, password, session)){
					session.setAttribute("username", username);
					view = "redirect:/";
				}else{
					view = "loginError";
				}		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return view;
	}
	
	@RequestMapping("/registration") //FINIRE LA REGISTRATI
	protected String getRegistration(
			@RequestParam(value = "nome", required = false) String nome,
			@RequestParam(value = "cognome", required = false) String cognome,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "indirizzo", required = false) String indirizzo,
			@RequestParam(value = "citta", required = false) String citta,
			@RequestParam(value = "telefono", required = false) String telefono,
			@RequestParam(value = "password", required = false) String password,
			HttpServletRequest request){
		
		data.setNome(nome);
		data.setCognome(cognome);
		data.setEmail(email);
		data.setIndirizzo(indirizzo);
		data.setCitta(citta);
		data.setTelefono(telefono);
		data.setPassword(password);
		
		String view = null;
		try {
				HttpSession session = request.getSession();
				if(cmFacade.getCustomerServiceForRegistration(data)){
					view = "welcome";
				}else{
					view = "registrationError";
				}		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return view;
	}
}
