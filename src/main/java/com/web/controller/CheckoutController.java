package com.web.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.core.model.ProductModel;
import com.facade.data.CustomerData;
import com.facade.facades.CustomerFacade;
import com.facade.facades.ItemsFacade;
import com.facade.facades.ProductFacade;

@Controller
@RequestMapping("/checkoutController")
public class CheckoutController{

	@Resource(name="itemsFacade")
	private ItemsFacade itemsFacade;
	
	@Resource(name="productFacade")
	private ProductFacade productFacade;
	
	@Resource(name="customerFacade")
	private CustomerFacade customerFacade;
	
	@Resource(name="customerData")
	private CustomerData customer;
	
	
	@RequestMapping("/checkout")
	protected ModelAndView getCheckoutInfo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		ModelAndView mw = null;
		if(session.getAttribute("idCustomer")!=null){
			int customerID = (int) session.getAttribute("idCustomer");

			double totaleParziale = 0;
			int nItems = 0;
			try {
				customer = customerFacade.getCustomerServiceForCheckout(customerID); 
				totaleParziale = productFacade.getPartialTotalProducts(customerID);
				nItems = itemsFacade.getCartService(customerID);
				mw = new ModelAndView("checkout");
				mw.addObject("customerData", customer);
				mw.addObject("totaleParziale", totaleParziale);
				mw.addObject("nItems", nItems);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			mw = new ModelAndView("redirect:/loginController/login");
		}
		return mw;
	}
	
	@RequestMapping("/checkoutConfirm")
	protected String confirmAndCloseCart(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("idCustomer")!=null){
			int customerID = (int) session.getAttribute("idCustomer");
			try {
				productFacade.getProductServiceForClosingCart(customerID);
				return "checkoutConfirmation";
			} catch (SQLException e) {
				return "checkout";
			}
			
		}else{
			return "redirect:/loginController/login";
		}
	}

}
