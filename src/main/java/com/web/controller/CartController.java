package com.web.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.facade.data.CartData;
import com.facade.facades.ItemsFacade;
import com.facade.facades.ProductFacade;

@Controller
@RequestMapping("/cartController")
public class CartController{

	@Resource(name="productFacade")
	private ProductFacade pFacade;
	
	@Resource(name="itemsFacade")
	private ItemsFacade itemsFacade;

	@RequestMapping("/cartSummary")
	protected ModelAndView getCartSummary (HttpServletRequest request){
		// TODO Auto-generated method stub
		ModelAndView mw = null;
		HttpSession session = request.getSession();
		if(session.getAttribute("idCustomer")!=null){
			int customerID = (int) session.getAttribute("idCustomer");
			
			ArrayList<CartData> productsList = null;
			
			try {
				productsList = pFacade.getProductServiceByCustomer(customerID);
				mw = new ModelAndView("cart");
				mw.addObject("productsList", productsList);
				mw.addObject("nItems", productsList.size());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			mw = new ModelAndView("redirect:/loginController/login");
		}
		
		return mw;
	}
	
	@RequestMapping(value = "/addOrRemoveToCart", method = RequestMethod.POST)
	protected void getCatalogByCategory(
			@RequestParam("action") String action,
			@RequestParam("id_cartItem") String id_cartItem,
			HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session.getAttribute("idCustomer")!=null){
			int quantity=0;
			try {
				quantity = itemsFacade.getProductServiceByCartItem(Integer.parseInt(id_cartItem), action);
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.setContentType("text/plain");
			response.getWriter().write(String.valueOf(quantity));
		}else{
			goToLogin();
		}
	}
	
	@RequestMapping(value = "/deleteCart", method = RequestMethod.POST)
	protected void deleteAllProducts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session.getAttribute("idCustomer")!=null){
			int customerID = (int) session.getAttribute("idCustomer");
			try {
				pFacade.getProductServiceForDelete(customerID);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			goToLogin();
		}
	}
	
	private static String goToLogin(){
		return "redirect:/loginController/login";
	}
}
