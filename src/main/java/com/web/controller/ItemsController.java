package com.web.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.facade.facades.ItemsFacade;

@Controller
public class ItemsController extends HttpServlet {

	@Resource(name="itemsFacade")
	private ItemsFacade itemFacade;

	@RequestMapping(value = "/itemsController", method = RequestMethod.POST)
	protected void addItemIntoCart(
			@RequestParam("productID") String product,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session.getAttribute("idCustomer")!=null){
			int productID = Integer.parseInt(product);
			int customerID = (int) session.getAttribute("idCustomer");
			int nItemsOfCart = 0;
			try {
				nItemsOfCart = itemFacade.getCartService(productID, customerID);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.setContentType("text/plain");
			response.getWriter().write(String.valueOf(nItemsOfCart));
		}else{
			goToLogin();
		}
	}
	
	private static String goToLogin(){
		return "redirect:/loginController/login";
	}
}
