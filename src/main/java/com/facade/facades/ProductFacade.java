package com.facade.facades;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;

import com.core.model.ProductModel;
import com.core.service.ProductService;
import com.facade.data.CartData;
import com.facade.populator.CartConverter;

public class ProductFacade{
	@Resource(name="productService")
	private ProductService productService;
	
	@Resource(name="cartConverter")
	private CartConverter populator;
	
	public ArrayList<ProductModel> getProductServiceByCategory(String category) throws SQLException{
		ArrayList<ProductModel> list = null;
		list = productService.getCatalogsByCategory(category);
		return list;
	}
	
	public ArrayList<CartData> getProductServiceByCustomer(int customerID) throws SQLException{
		return populator.convert(productService.getSummaryCart(customerID));
	}
	
	public boolean getProductServiceForDelete(int customerID) throws SQLException {
	return productService.removeCartByCustomer(customerID);
}
	
	public double getPartialTotalProducts(int customerID) throws SQLException{
		ArrayList<CartData> list = populator.convert(productService.getSummaryCart(customerID));
		double totale=0;
		double prezzo;
		int quantity;
		for(int i=0; i<list.size();i++){
			prezzo = list.get(i).getPrezzo();
			quantity = list.get(i).getQuantita();
			totale = totale + (prezzo*quantity);
		}
		return totale;
	}

	public boolean getProductServiceForClosingCart(int customerID) throws SQLException {
		return productService.closeCartByCustomer(customerID);
	}
}
