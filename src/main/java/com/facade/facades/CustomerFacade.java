package com.facade.facades;

import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.core.model.CustomerModel;
import com.core.service.CustomerService;
import com.facade.data.CustomerData;
import com.facade.populator.CustomerConverter;

public class CustomerFacade {
	@Resource(name="customerService")
	private CustomerService customerService;
	
	@Resource(name="customerConverter")
	private CustomerConverter populator;
	
	public boolean getCustomerServiceForLogin(String username, String password, HttpSession session) throws SQLException{
		return customerService.getCustomerForLogin(username, password, session);
	}
	
	public boolean getCustomerServiceForRegistration(CustomerData data) throws SQLException{
		return customerService.addCustomer(populator.convert(data));
	}

	public CustomerData getCustomerServiceForCheckout(int customerID) throws SQLException {
		return populator.convert(customerService.getCustomerForCheckout(customerID));
	}
}
