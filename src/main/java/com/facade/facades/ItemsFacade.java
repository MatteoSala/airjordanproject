package com.facade.facades;

import java.sql.SQLException;

import javax.annotation.Resource;

import com.core.service.ItemsService;

public class ItemsFacade {

	@Resource(name = "itemsService")
	private ItemsService itemsService;
	
	public int getCartService(int productID, int customerID) throws SQLException{
		return itemsService.addToCart(productID,customerID);
		
	}
	
	public int getCartService(int customerID) throws SQLException{
		return itemsService.getNCartItems(customerID);
		
	}

	public int getProductServiceByCartItem(int id_cartItem, String action) throws SQLException {
		return itemsService.getQuantityByIdCartItem(id_cartItem, action);
	}
}
