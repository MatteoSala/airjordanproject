package com.facade.populator;

import java.util.ArrayList;

import javax.annotation.Resource;
import org.springframework.core.convert.converter.Converter;
import com.core.model.CartModel;
import com.facade.data.CartData;

public class CartConverter implements Converter<ArrayList<CartModel>, ArrayList<CartData>>{

	@Resource(name="cartData")
	private CartData data;

	@Override
	public ArrayList<CartData> convert(ArrayList<CartModel> model) {
		ArrayList<CartData> list = new ArrayList<CartData>();
		for(int i=0; i<model.size(); i++){
			data = new CartData();
			data.setId(model.get(i).getId());
			data.setDescrizione(model.get(i).getDescrizione());
			data.setFoto(model.get(i).getFoto());
			data.setPrezzo(model.get(i).getPrezzo());
			data.setQuantita(model.get(i).getQuantita());
			list.add(data);
		}
		
		return list;
	}

}
