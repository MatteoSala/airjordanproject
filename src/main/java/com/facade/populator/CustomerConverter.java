package com.facade.populator;

import javax.annotation.Resource;

import org.springframework.core.convert.converter.Converter;

import com.core.model.CustomerModel;
import com.core.service.HashingPasswordMD5;
import com.facade.data.CustomerData;

public class CustomerConverter implements Converter<CustomerData, CustomerModel>{

	@Resource(name="customerModel")
	private CustomerModel model;
	
	@Resource(name="customerData")
	private CustomerData customerData;
	
	@Resource(name="hashingMD5")
	private HashingPasswordMD5 encrypt;
	
	@Override
	public CustomerModel convert(CustomerData data) {
		model.setNome(data.getNome());
		model.setCognome(data.getCognome());
		model.setEmail(data.getEmail());
		model.setIndirizzo(data.getIndirizzo());
		model.setCitta(data.getCitta());
		model.setTelefono(data.getTelefono());
		model.setPassword(encrypt.hashingMD5(data.getPassword()));
		
		return model;
	}

	public CustomerData convert(CustomerModel customerModel) {
		customerData.setNome(customerModel.getNome());
		customerData.setCognome(customerModel.getCognome());
		customerData.setEmail(customerModel.getEmail());
		customerData.setIndirizzo(customerModel.getIndirizzo());
		customerData.setCitta(customerModel.getCitta());
		customerData.setTelefono(customerModel.getTelefono());
		
		return customerData;
	}

}
