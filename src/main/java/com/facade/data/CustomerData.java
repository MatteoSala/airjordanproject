package com.facade.data;

public class CustomerData {

		private String nome;
		private String cognome;
		private String email;
		private String indirizzo;
		private String citta;
		private String telefono;
		private String password;
		
		public CustomerData(){}
		
		
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public String getCognome() {
			return cognome;
		}
		public void setCognome(String cognome) {
			this.cognome = cognome;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getIndirizzo() {
			return indirizzo;
		}
		public void setIndirizzo(String indirizzo) {
			this.indirizzo = indirizzo;
		}
		public String getCitta() {
			return citta;
		}
		public void setCitta(String citta) {
			this.citta = citta;
		}
		public String getTelefono() {
			return telefono;
		}
		public void setTelefono(String telefono) {
			this.telefono = telefono;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		@Override
		public String toString() {
			return "CustomerData [nome=" + nome + ", cognome=" + cognome + ", email=" + email + ", indirizzo="
					+ indirizzo + ", citta=" + citta + ", telefono=" + telefono + ", password=" + password + "]";
		}
		
		
}
